import QtQuick 2.13
import QtQuick.Controls 2.12
import QtQuick.Window 2.13

Window
{
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    TextField
    {
        id : textfield

        x : 100;    y : 100;
        width : 100;    height : 50;
        placeholderText: "Enter"
    }

    Button
    {
        id : button;

        x : 0;  y : 0;
        width : 100;    height : 30;
        text : "SET"

        onClicked:
        {

        }
    }
}
